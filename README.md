# Numerical Methods

Algorithms of Numerical Methods implemented in Python

This repository contains Python modules for

- `Euler_Method.py` : Solving ordinary differential equations using Forward & Backward Euler Methods
- `Gauss_Legendre_Quadrature.py` : Integrate a function using Gaussian Legendre Quadrature
- `Jacobi_Method_Eigenvalues.py` : Find eigenvalues and their eigenvectors of $`n`$-dimensional square matrices using Jacobi Method
- `Jacobi_Method_SLE.py` : Solving system of linear equations using Jacobi method
- `NDDF_Differentiation.py` : Differentiate a given data using Newton’s Divided Difference Formulation
- `Runge_Kutta.py` : Solving Ordinary Differential Equation using $`3^{rd}`$ or $`4^{th}`$ Order Runge Kutta Methods
- `Simpsons_Rule.py` : Integrate a function using Simpson's $`1/3^{rd}`$, $`3/8^{th}`$ and corresponding Composite Rules
- `Two_Point_Gauss_Quadrature.py` : Integrate a function using Two-Point Gaussian Quadrature


