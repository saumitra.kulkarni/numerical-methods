# -*- coding: utf-8 -*-
"""
@author: Saumitra Kulkarni
"""
import numpy as np

# Find largest off-diag. element a[k,l]
def maxElem(a):
    """
    Find largest off-diagonal element for square matrix a
    
    Parameters
    ----------
    a : ndarray
        n-dimensional square matrix.

    Returns
    -------
    max_val : float
        largest off-diagonal element.
    k : int
        Row index of largest off-diagonal element.
    l : int
        Column index of largest off-diagonal element.

    """
    n = len(a)
    max_val = 0.0
    for i in range(n-1):
        for j in range(i+1, n):
            if abs(a[i,j]) >= max_val:
                max_val = abs(a[i,j])
                k = i
                l = j
    return max_val, k, l

def rotate(a, p, k, l): 
    """
    Rotate matrix a to make a[k,l] = 0

    Parameters
    ----------
    a : ndarray
        eigenvector matrix.
    p : ndarray
        eigenvalues matrix.
    k : int
        Row index of maximum value.
    l : int
        Column index of maximum value.

    Returns
    -------
    a : ndarray
        Eigenvalues matrix.
    p : ndarray
        Eigenvector matrix.
        
    """
    n = len(a)
    aDiff = a[l,l] - a[k,k]
    
    if abs(a[k,l]) < abs(aDiff)*1.0e-36: 
        t = a[k,l]/aDiff
    else:
        phi = aDiff/(2.0*a[k,l])
        t = 1.0/(abs(phi) + np.sqrt(phi**2 + 1.0))
        if phi < 0.0: 
            t = -t
    
    c = 1.0/np.sqrt(t**2 + 1.0)
    s = t*c
    tau = s/(1.0 + c)
    temp = a[k,l]
    
    a[k,l] = 0.0
    a[k,k] = a[k,k] - t*temp
    a[l,l] = a[l,l] + t*temp
    
    # Case where elements of array are i < k
    for i in range(k):      
        temp = a[i,k]
        a[i,k] = temp - s*(a[i,l] + tau*temp)
        a[i,l] = a[i,l] + s*(temp - tau*a[i,l])
    # Case where elements of array are k < i < l
    for i in range(k+1, l):  
        temp = a[k,i]
        a[k,i] = temp - s*(a[i,l] + tau*a[k,i])
        a[i,l] = a[i,l] + s*(temp - tau*a[i,l])
    # Case where elements of array are i > l
    for i in range(l+1, n):  
        temp = a[k,i]
        a[k,i] = temp - s*(a[l,i] + tau*temp)
        a[l,i] = a[l,i] + s*(temp - tau*a[l,i])
    # Update eigenvalues matrix
    for i in range(n):      
        temp = p[i,k]
        p[i,k] = temp - s*(p[i,l] + tau*p[i,k])
        p[i,l] = p[i,l] + s*(temp - tau*p[i,l])
     
    return a, p

# Jacobi method
def jacobi_method_eigen(a, tol = 1.0e-9):
    """
    Jacobi Method for Eigenvalues and Eigenvectors

    Parameters
    ----------
    a : ndarray
        n-dimensional square matrix.
    tol : float, optional
        tolerance for maximum value of off-diagonal elements. 
        The default is 1.0e-9.

    Raises
    ------
    TypeError
        Only square matrices are allowed.

    Returns
    -------
    eigenvalues : ndarray
        Eigenvalues matrix
    eigenvectors : TYPE
        Eigenvalues matrix.
        
    """
    if a.shape[0] != a.shape[1]:
        raise TypeError("Only square matrices are allowed") 
    # Copy array a to eigenvalues
    eigenvalues = a.copy()
    # Get number of eigenvalues
    n = len(eigenvalues)
    # Set maximum number of rotations
    maxRot = 5*(n**2)
    # Initialize matrix for eigenvectors
    eigenvectors = np.identity(n)*1.0   
    # Jacobi rotation loop 
    for i in range(maxRot): 
        max_val, k, l = maxElem(eigenvalues)
        if max_val < tol: 
            return np.diagonal(eigenvalues), eigenvectors
        eigenvalues, eigenvectors = rotate(eigenvalues, eigenvectors, k, l)
        
    print('Jacobi method did not converge')
    
    return eigenvalues, eigenvectors

# Driver code
if __name__ == '__main__':
    a = np.array([[1, np.sqrt(2), 2],[np.sqrt(2), 3, np.sqrt(2)],[2, np.sqrt(2), 1]])
    eigenvalues, eigenvectors = jacobi_method_eigen(a, tol = 1.0e-9)
    print('\nMatrix\n\n', a)
    print('\nEigenvalues \t Eigenvectors\n')
    for i in range(len(eigenvalues)):
        print(eigenvalues[i],'\t\t\t',eigenvectors[:,i])
    
